/*:
 * @plugindesc Permits the customization of menu screens
 * @author CrawlorSoft
 *
 * @param IncludeDefaultTitleMenu
 * @desc Whether or not to include the default title menu items in the title menu.
 * @default true
 *
 * @param AddTestMenuItems
 * @desc Whether or not to add simple test menu items to each available menu to verify the plugin works as expected.
 * @default false
 *
 * @help
 *
 * CrawlorSoft Menu Customization Plugin
 *
 * This top level plugin is used to permit the addition of menu items by other plugins.  It provides the ability to
 * add new menu items in addition to the default menu items or completely override the menu to only display items
 * added through the CrawlorSoft Menu Customization plugin.
 *
 * Currently supported menus for entry customization are:
 *
 * 1. The main title menu
 *
 * Warning:  This plugin overrides the entirety of the create for the menu windows and command list for supported menus.
 *
 * Usage:  Add a credit line for CrawlorSoft to your game
 *
 * Version 1.1.0
 * Copyright CrawlorSoft May 2018
 *
 * Last Updated:  January 2019
 */
// jshint -W097
"use strict";
// jshint -W132
var CSFT = CSFT || {};
// jshint +W132
CSFT.CustomizeableMenus = CSFT.CustomizeableMenus || {};

(function(_) {
    let pluginParameters = PluginManager.parameters('CustomizeableMenus');
    pluginParameters.IncludeDefaultTitleMenu = pluginParameters.IncludeDefaultTitleMenu.trim().toLowerCase() === "true";
    pluginParameters.AddTestMenuItems = pluginParameters.AddTestMenuItems.trim().toLowerCase() === "true";

    /////// CrawlorSoft Menu Window Creation - START /////////
    _.generateMenuWindow = function(subMenuItems, CSWindow_Object) {
        if (subMenuItems.constructor !== Array) {
            alert("subMenuItems must be an array of CrawlorSoftMenuEntryItem objects.");
        }
        if (subMenuItems.length > 0 && subMenuItems[0].constructor != CrawlorSoftMenuEntryItem) {
            alert("subMenuItems[0] must be a CrawlorSoftMenuEntryItem object.");
        }

        CSWindow_Object.prototype = Object.create(Window_Command.prototype);
        CSWindow_Object.prototype.constructor = CSWindow_Object;
        CSWindow_Object.prototype.subMenuItems = subMenuItems;

        CSWindow_Object.prototype.initialize = function() {
            Window_Command.prototype.initialize.call(this, 0, 0);
            this.updatePlacement();
        };
        CSWindow_Object.prototype.windowWidth = function() {
            return 400;
        };
        CSWindow_Object.prototype.windowHeight = function() {
            return this.fittingHeight(Math.min(this.numVisibleRows(), 12));
        };
        CSWindow_Object.prototype.updatePlacement = function() {
            this.x = (Graphics.boxWidth - this.width) / 2;
            this.y = (Graphics.boxHeight - this.height) / 2;
        };
        CSWindow_Object.prototype.makeCommandList = function() {
            let commands = CSWindow_Object.prototype.subMenuItems;
            for (let idx in commands) {
                this.addCommand(commands[idx].text, commands[idx].key, commands[idx].handler, commands[idx].condition);
            }
        };
        CSWindow_Object.prototype.drawItem = function(index) {
            var rect = this.itemRectForText(index);
            var statusWidth = this.statusWidth();
            var titleWidth = rect.width - statusWidth;
            this.resetTextColor();
            this.changePaintOpacity(this.isCommandEnabled(index));
            this.drawText(this.commandName(index), rect.x, rect.y, titleWidth, 'left');
            this.drawText(this.statusText(index), titleWidth, rect.y, statusWidth, 'right');
        };
        CSWindow_Object.prototype.statusWidth = function() {
            return 120;
        };
        CSWindow_Object.prototype.statusText = function(index) {
            var symbol = this.commandSymbol(index);
            var value = this.getConfigValue(symbol);
            // TODO:  Do something per rpg_windows.js line 2699
        };
        CSWindow_Object.prototype.getConfigValue = function(symbol) {
            return ConfigManager[symbol];
        };
        CSWindow_Object.prototype.setConfigValue = function(symbol, value) {
            ConfigManager[symbol] = value;
        };
    };
    /////// CrawlorSoft Menu Window Creation - END /////////

    /////// CrawlorSoft Menu Scene Creation - START /////////
    _.generateMenuScene = function(menuWindow, CSScene_Object) {
        CSScene_Object.prototype = Object.create(Scene_MenuBase.prototype);
        CSScene_Object.prototype.constructor = CSScene_Object;

        CSScene_Object.prototype.initialize = function() {
            Scene_MenuBase.prototype.initialize.call(this);
        };
        CSScene_Object.prototype.create = function() {
            Scene_MenuBase.prototype.create.call(this);
            this._crawlorsoftWindow = new menuWindow();
            this._crawlorsoftWindow.setHandler('cancel', this.popScene.bind(this));
            this.addWindow(this._crawlorsoftWindow);
        };
        CSScene_Object.prototype.crawlorsoftMenuCommand = function() {
            this._commandWindow.close();
            SceneManager.push(CSScene_Object);
        };
    };
    /////// CrawlorSoft Menu Scene Creation - END /////////

    /**
     * A basic class for a menu item which contains the necessary information to enable it and link command handlers.
     * Also contains methods to retrieve the command handler and the menu entry data for populating the menus.
     */
    class CrawlorSoftMenuEntryItem {
        constructor(text, key, handler, condition) {
            this.text = text;
            this.key = key;
            this.handler = handler;
            this.condition = condition;
        }

        getCommandData() {
            return {key: this.key, callback: this.handler};
        }

        getMenuEntry() {
            return {text: this.text, key: this.key, condition: this.condition};
        }
    }

    /**
     * Holds the entry information for the menus.
     */
    class CrawlorSoftMenu {
        constructor(defaultEntries) {
            this.entries = defaultEntries || [];
        }
        getEntries() {
            return this.entries;
        }
    }

    _.CrawlorSoftMenu = CrawlorSoftMenu;
    _.CrawlorSoftMenuEntryItem = CrawlorSoftMenuEntryItem;

    /**
     * Initializes the title menu structure and returns it.
     * @returns {CrawlorSoftMenu} A CrawlorSoftMenu structure with the default entries for the title menu.
     */
    _.initializeTitleMenu = function() {
        if (pluginParameters.IncludeDefaultTitleMenu) {
            return new CrawlorSoftMenu([
                new CrawlorSoftMenuEntryItem('New Game', 'newGame', Scene_Title.prototype.commandNewGame),
                new CrawlorSoftMenuEntryItem('Continue', 'continue', Scene_Title.prototype.commandContinue,
                    Window_TitleCommand.prototype.isContinueEnabled()),
                new CrawlorSoftMenuEntryItem('Options', 'options', Scene_Title.prototype.commandOptions)
            ]);
        }
        return new CrawlorSoftMenu([]);
    };

    /**
     * Data structure to hold the customizeable menu items.
     * @type {{titleMenu: CrawlorSoftMenu}}
     * @private
     */
    _._menus = {
        titleMenu: _.initializeTitleMenu()
    };

    /**
     * Adds a new item to the Title Menu (CrawlorSoftMenu).  If the condition is null or undefined, the item will
     * always be displayed.
     * @param key The key to add for the linking between the menu and the command to execute.
     * @param method The method to call when the menu item is selected.
     * @param menuText The text to display for the menu.
     * @param condition The condition (if any) which determines if the menu item is shown.
     * @param idx The index in the menu list to put the item.
     */
    _.addTitleMenuEntry = function(key, method, menuText, condition, idx) {
        let menuItem = new CrawlorSoftMenuEntryItem(menuText, key, method, condition);
        if (idx) {
            this._menus.titleMenu.entries.splice(idx, 0, menuItem);
        } else {
            this._menus.titleMenu.entries.push(menuItem);
        }
    };

    /**
     * Fetches an array of menu entries which contains the menu text, and the symbol to link against for handling.
     * @returns {Array} An array of menu entries.
     */
    _.getTitleMenuEntries = function() {
        let menuEntries = [];
        let data = this._menus.titleMenu.getEntries();
        for (let idx = 0; idx < data.length; idx++) {
            menuEntries.push(data[idx].getMenuEntry());
        }

        return menuEntries;
    };

    /**
     * Fetches an array of menu commands which are linked via the symbol on item creation to the menu entries.
     * @returns {Array} An array of menu commands.
     */
    _.getTitleMenuCommands = function() {
        let menuCommands = [];
        let data = this._menus.titleMenu.getEntries();

        for (let idx = 0; idx < data.length; idx++) {
            menuCommands.push(data[idx].getCommandData());
        }
        return menuCommands;
    };

    _.createTitleSubMenu = function(menuName, menuSymbol, subMenuItems) {
        if (subMenuItems.constructor === Array) {
            alert('subMenuItems must be an array of CrawlorSoftMenuItem entries.');
        }
    };

    /**
     * Overrides the Scene_Title.prototype.createCommandWindow function in rpg_scenes.js
     * This method cannot call the super method as it makes use of internal data structures to generate the menu.
     */
    Scene_Title.prototype.createCommandWindow = function() {
        this._commandWindow = new Window_TitleCommand();
        let commandHandlers = _.getTitleMenuCommands();
        for (let idx = 0; idx < commandHandlers.length; idx++) {
            this._commandWindow.setHandler(commandHandlers[idx].key, commandHandlers[idx].callback.bind(this));
        }
        this.addWindow(this._commandWindow);
    };

    /**
     * Overrides the Window_TitleCommand.prototype.makeCommandList function in rpg_windows.js
     * This method cannot call the super method as it makes use of internal data structures to generate the menu.
     */
    Window_TitleCommand.prototype.makeCommandList = function() {
        let items = _.getTitleMenuEntries();
        for (let idx = 0; idx < items.length; idx++) {
            this.addCommand(items[idx].text, items[idx].key, items[idx].condition);
        }
    };

    function Scene_TestScene() {
        this.initialize.apply(this, arguments);
    }
    function Window_TestWindow() {
        this.initialize.apply(this, arguments);
    }

    // If and only if the test mode is enabled, add test menu items to the menus which output a log.
    if (pluginParameters.AddTestMenuItems) {
        // Test Command Entry
        _.commandLog = function() {
            console.log('HELLO RPG MAKER MV WORLD!');
        };

        _.addTitleMenuEntry('testEntry', _.commandLog, 'Output A Log', true, 1);

        let registrationMenuEntry = new CSFT.CustomizeableMenus.CrawlorSoftMenuEntryItem('Test Command 1', 'cstc1', _.commandLog, null);
        let loginMenuEntry = new CSFT.CustomizeableMenus.CrawlorSoftMenuEntryItem('Test Command 2', 'cstc2', null, null);
        let logoutMenuEntry = new CSFT.CustomizeableMenus.CrawlorSoftMenuEntryItem('Test Command 3', 'cstc3', _.commandLog, null);

        let gameSparksMenuEntries = [
            registrationMenuEntry,
            loginMenuEntry,
            logoutMenuEntry
        ];

        CSFT.CustomizeableMenus.generateMenuWindow(gameSparksMenuEntries, Window_TestWindow);
        CSFT.CustomizeableMenus.generateMenuScene(Window_TestWindow, Scene_TestScene);

        CSFT.CustomizeableMenus.addTitleMenuEntry('GamesparksIntegrationMenu', Scene_TestScene.prototype.crawlorsoftMenuCommand, 'Test Sub-Menu', true, 1);
    }

})(CSFT.CustomizeableMenus);
