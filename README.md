# CrawlorSoft Customizeable Title Menu Plugin

Version 1.0.0

Created By: CrawlorSoft

Copyright May 2018

## Overview

This plugin is intended to allow the modification of the main title menu for RPG Maker MV games.

## Enabling

1. Place the *CustomizeableMenus.js* file into your ***/&lt;game&gt;/js/plugins*** folder.
2. Click on the Plugin Manager (puzzle piece) icon in the toolbar.
3. Double-click an available plugin slot, and select CustomizeableMenus from the name drop-down.
4. Change the status to *ON*.

Your game will now be using the customizeable menus plugin, however, it will still have the default menus.

## Disabling Default Title Menu Items

At this time, all default title menu items must be disabled at once.  To disable them (assuming you have enabled the plugin):

1. Click on the Plugin Manager (puzzle piece) icon in the toolbar.
2. Double-click on the *CustomizeableMenus* entry.
3. Double-click on the *IncludeDefaultTitleMenu* entry under the **Parameters** section.
4. Change *true* to *false*.

## Verifying The Plugin Is Working

To test that the customizeable menus plugin is working correctly (assuming you have enabled it as described above):

1. Click on the Plugin Manager (puzzle piece) icon in the toolbar.
2. Double-click on the *CustomizeableMenus* entry.
3. Double-click on the *IncludeDefaultTitleMenu* entry under the **Parameters** section.
4. Change *true* to *false*.
3. Double-click on the *AddTestMenuItems* entry under the **Parameters** section.
4. Change *false* to *true*.

Run a play test of your game, and press the ***F12*** key to bring up the console window.  Now double click on the
***Output A Log*** entry and you should see the line: `HELLO RPG MAKER MV WORLD` appear.


